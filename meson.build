project('gnome-builder-git', 'c',
          license: 'GPL2+',
          version: '3.33.0',
    meson_version: '>= 0.50.0',
  default_options: [ 'c_std=gnu11', 'warning_level=2' ],
)

cc = meson.get_compiler('c')
global_c_args = []
test_c_args = [
  '-Wcast-align',
  '-Wdeclaration-after-statement',
  '-Werror=address',
  '-Werror=array-bounds',
  '-Werror=empty-body',
  '-Werror=implicit',
  '-Werror=implicit-function-declaration',
  '-Werror=incompatible-pointer-types',
  '-Werror=init-self',
  '-Werror=int-conversion',
  '-Werror=int-to-pointer-cast',
  '-Werror=main',
  '-Werror=misleading-indentation',
  '-Werror=missing-braces',
  '-Werror=missing-include-dirs',
  '-Werror=nonnull',
  '-Werror=overflow',
  '-Werror=parenthesis',
  '-Werror=pointer-arith',
  '-Werror=pointer-to-int-cast',
  '-Werror=redundant-decls',
  '-Werror=return-type',
  '-Werror=sequence-point',
  '-Werror=shadow',
  '-Werror=strict-prototypes',
  '-Werror=trigraphs',
  '-Werror=undef',
  '-Werror=write-strings',
  '-Wformat-nonliteral',
  ['-Werror=format-security', '-Werror=format=2' ],
  '-Wignored-qualifiers',
  '-Wimplicit-function-declaration',
  '-Wlogical-op',
  '-Wmissing-format-attribute',
  '-Wmissing-include-dirs',
  '-Wmissing-noreturn',
  '-Wnested-externs',
  '-Wno-cast-function-type',
  '-Wno-missing-field-initializers',
  '-Wno-sign-compare',
  '-Wno-unused-parameter',
  '-Wold-style-definition',
  '-Wpointer-arith',
  '-Wredundant-decls',
  '-Wstrict-prototypes',
  '-Wswitch-default',
  '-Wswitch-enum',
  '-Wundef',
  '-Wuninitialized',
  '-Wunused',
  '-fno-strict-aliasing',
]
foreach arg: test_c_args
  if cc.has_multi_arguments(arg)
    global_c_args += arg
  endif
endforeach
add_project_arguments(global_c_args, language: 'c')

gnome = import('gnome')

libgio_unix_dep = dependency('gio-unix-2.0', version: '>= 2.60.0')
libgit_glib_dep = dependency('libgit2-glib-1.0', version: '>= 0.27.8')

gnome_builder_git_deps = [
  libgio_unix_dep,
  libgit_glib_dep,
]

change_monitor_src = gnome.gdbus_codegen('ipc-git-change-monitor',
           sources: 'org.gnome.Builder.Git.ChangeMonitor.xml',
  interface_prefix: 'org.gnome.Builder.',
         namespace: 'Ipc',
)

config_src = gnome.gdbus_codegen('ipc-git-config',
           sources: 'org.gnome.Builder.Git.Config.xml',
  interface_prefix: 'org.gnome.Builder.',
         namespace: 'Ipc',
)

progress_src = gnome.gdbus_codegen('ipc-git-progress',
           sources: 'org.gnome.Builder.Git.Progress.xml',
  interface_prefix: 'org.gnome.Builder.',
         namespace: 'Ipc',
)

repository_src = gnome.gdbus_codegen('ipc-git-repository',
           sources: 'org.gnome.Builder.Git.Repository.xml',
  interface_prefix: 'org.gnome.Builder.',
         namespace: 'Ipc',
)

service_src = gnome.gdbus_codegen('ipc-git-service',
           sources: 'org.gnome.Builder.Git.Service.xml',
  interface_prefix: 'org.gnome.Builder.',
         namespace: 'Ipc',
)

gnome_builder_git_sources = [
  'gnome-builder-git.c',
  'ipc-git-config-impl.c',
  'ipc-git-change-monitor-impl.c',
  'ipc-git-index-monitor.c',
  'ipc-git-remote-callbacks.c',
  'ipc-git-repository-impl.c',
  'ipc-git-service-impl.c',
  'line-cache.c',
  change_monitor_src,
  config_src,
  progress_src,
  repository_src,
  service_src,
]

gnome_builder_git = executable('gnome-builder-git', gnome_builder_git_sources,
           install: true,
       install_dir: get_option('libexecdir'),
      dependencies: gnome_builder_git_deps,
)

test_git_sources = [
  'test-git.c',
  change_monitor_src,
  config_src,
  progress_src,
  repository_src,
  service_src,
]

test_git = executable('test-git', 'test-git.c', test_git_sources,
  dependencies: [ libgio_unix_dep ],
)
test('test-git', test_git)
