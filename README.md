# gnome-builder-git

This is a GPLv2.0+ licensed daemon that wraps libgit2-glib and libgit2.
It provides some of the services that are necessary for an IDE using Git.

Note that libgit2 is GPLv2 only with a Linking Exception which can be found in the [https://github.com/libgit2/libgit2/blob/master/COPYING](libgit2 Git repository).
Also, libgit2-glib, which wraps the libgit2 library, is licensed under LGPLv2.1+ and can be found in the [https://gitlab.gnome.org/GNOME/libgit2-glib/raw/master/COPYING](libgit2-glib Git repository).

The design is similar to Language Server Protocol, in that RPCs are performed over stdin/stdout to a subprocess daemon.
However, we use the DBus serialization format instead of JSON/JSONRPC for various reasons including efficiency and design.
For example, we can extend our implementation to trivially include FD passing for future extensions.
Unlike JSONRPC, we have gdbus-codegen to generate the IPC stubs giving us a clean, minimal daemon implementation and convenient APIs for the consumer.
All of the RPCs are defined in the various DBus interface description XML files.
